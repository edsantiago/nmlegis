package NMLegis::UI::Controller::Auth;
use Mojo::Base 'Mojolicious::Controller', -signatures;

use Crypt::PBKDF2;

sub login_form ($self) {
    if ($self->session('userid')) {
        # FIXME: what's a good way to handle this?
        $self->flash(message => 'Um, you\'re already logged in');
    }

    # Try to take user back to the page they came from
    if (my $referer = $self->req->headers->referer) {
        $self->stash(referer => $referer);
    }

    $self->render();
}

# POST
sub login ($self) {
    my $email    = $self->param('email');
    my $given_password = $self->param('password');

    my $dbix = $self->app->db->{_dbix};
    $dbix->query("SELECT id, password FROM users WHERE email=? COLLATE NOCASE", $email)->into(my ($userid, $hashed_password));

    my $pbkdf2 = Crypt::PBKDF2->new;
    if ($email && $hashed_password && $pbkdf2->validate($hashed_password, $given_password)) {
        $self->session(userid => $userid);
        $self->session(expiration => 86400 * 365 * 2);
        $self->redirect_to($self->param('referer') || '/trackers');
    } else {
        $self->flash(message => 'Invalid email or password');
        $self->redirect_to('/login');
    }
}

sub logout ($self) {
    $self->session(userid => 0);
    $self->redirect_to('/');
}

sub register_form ($self) {
    if ($self->session('userid')) {
        # FIXME: what's a good way to handle this?
        $self->flash(error => 'Um, you\'re already registered and logged in');
        return $self->redirect_to('/');
    }

    $self->render();
}

sub register ($self) {
    my $v = $self->validation;

    return $self->render('auth/register_form')       unless $v->has_data;

    $v->required('email', 'trim')->size(5, 50)->like(qr/^\S+\@\S+\.\S+$/);
    $v->required('password')->size(7,100);
    $v->required('firstname', 'trim')->size(1,30)->like(qr/^\w[\w\s]+\w$/);
    $v->required('lastname', 'trim')->size(1,30)->like(qr/^\w[\w\s]+\w$/);
    $v->optional('hdistrict','trim')->num(1,70);        # FIXME! hardcoding
    $v->optional('sdistrict','trim')->num(1,42);        # FIXME! hardcoding

    if ($v->has_error) {
        $self->flash(error => "Please address these errors and try again");
        my @errlist = (error => "Please address these errors and try again");
        if (my $failed = $v->failed) {
            #use Data::Dump; dd $failed;
            for my $field (@$failed) {
                my ($check, $result, @args) = @{ $v->error($field) };
                my $err = 'Invalid input';
                if ($check eq 'size') {
                    $err = "Must be $args[0]-$args[1] characters";
                }
                elsif ($check eq 'like') {
                    $err = 'Invalid input';
                }
                else {
                    warn "Hey Ed, FIXME, unknown check '$check' for $field";
                }
                # For Ed to debug
                warn "Registration failure: $field -> $err";
                push @errlist, "${field}_errors" => $err;
            }
        }
        return $self->render('auth/register_form', @errlist);
    }

    my $firstname      = $v->param('firstname');
    my $lastname       = $v->param('lastname');

    my $pbkdf2 = Crypt::PBKDF2->new;
    my $pwhash = $pbkdf2->generate($v->param('password'));

    my $dbix = $self->app->db->{_dbix};
    my @bind = (undef, $v->param('email'), $pwhash,
                map { $v->param($_) } qw(firstname lastname hdistrict sdistrict),
                0);
    $dbix->query("INSERT INTO users VALUES (??)", @bind);

    my $userid = $dbix->last_insert_id;
    $self->session(userid => $userid);
    $self->session(expiration => 86400 * 365 * 2);

    # Every new user gets a tracker
    my @trackernameoptions = ($firstname,
                              sprintf("%s%1.1s", $firstname, $lastname),
                              map { "${firstname}$_" } qw(1..10),
                              "$firstname$lastname"
                          );
    for my $tname (@trackernameoptions) {
        $dbix->query('SELECT id FROM trackers WHERE name=? COLLATE NOCASE', $tname)->into(my $found);
        if (! $found) {
            $dbix->query('INSERT INTO trackers VALUES (??)',
                         undef, $tname, "$firstname $lastname",
                         $self->user->id, 0, undef);
            return $self->render(tracker => $tname);
        }
    }

    # FIXME! ERROR!
}


1;
